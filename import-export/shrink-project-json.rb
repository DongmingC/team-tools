#!/usr/bin/env ruby

require 'json'
require 'yaml'
require 'date'

in_file = ARGV[0]
out_file = ARGV[1]

unless in_file && out_file
  usage = <<~MSG
  USAGE: <ENV_VAR>=<VAL> #{$PROGRAM_NAME} <in_file> <out_file>

  OPTIONS:
    RELATION_MAX_COUNT (int):      The maximum number of relations to retain
    RELATION_MAX_SIZE_BYTES (int): Size in bytes above which relations will be excluded
    RELATION_MAX_SIZE_SRELS (int): Number of sub-relations above which a parent relation will be excluded
    RELATION_MAX_AGE_MONTHS (int): Age in months beyond which a relation will be excluded

  EXAMPLE:
    RELATION_MAX_COUNT=100 #{$0} ./project.json ./shrunk-project.json
  MSG
  puts usage
  exit 1
end

relation_max_count = ENV.fetch('RELATION_MAX_COUNT', 0).to_i
relation_max_size_bytes = ENV.fetch('RELATION_MAX_SIZE_BYTES', 0).to_i
relation_max_size_srels = ENV.fetch('RELATION_MAX_SIZE_SRELS', 0).to_i
relation_max_age = ENV.fetch('RELATION_MAX_AGE_MONTHS', 0).to_i

json = JSON.parse(File.read(in_file))

def relations_count(object)
  case object
  when Hash
    1 + object.sum do |_, value|
      relations_count(value)
    end
  when Array
    object.sum do |item|
      relations_count(item)
    end
  else
    0
  end
end

def item_too_old?(item, max_age)
  relation_age_date = item['created_at']&.yield_self { |d| Date.parse(d) }
  relation_age_date && relation_age_date < (Date.today << max_age)
end

item_dates = []
json.each do |key, value|
  next unless value.is_a?(Array)

  item_count = 0

  value.select! do |item|
    size_in_bytes = item.to_json.size
    sub_relations_count = relations_count(item)

    item_dates << Date.parse(item['created_at'])

    if relation_max_size_bytes > 0 && size_in_bytes > relation_max_size_bytes
      puts "Skipping #{key} with ID: #{item['id']}, and IID: #{item['iid']}. Relation too large (#{size_in_bytes} bytes; max: #{relation_max_size_bytes})"
      false
    elsif relation_max_size_srels > 0 && sub_relations_count > relation_max_size_srels
      puts "Skipping #{key} with ID: #{item['id']}, and IID: #{item['iid']}. Relation too large (#{sub_relations_count} sub-relations; max: #{relation_max_size_srels})"
      false
    elsif relation_max_count > 0 && item_count > relation_max_count
      puts "Skipping #{key} with ID: #{item['id']}, and IID: #{item['iid']}. Max relations count reached (#{relation_max_count})"
      false
    elsif relation_max_age > 0 && item_too_old?(item, relation_max_age)
      puts "Skipping #{key} with ID: #{item['id']}, and IID: #{item['iid']}. Relation too old (#{item['created_at']})"
      false
    else
      item_count += 1
      true
    end
  end
end

item_dates.sort!

age_distribution = {
  p50: item_dates[0.5 * item_dates.size],
  p75: item_dates[0.75 * item_dates.size],
  p90: item_dates[0.9 * item_dates.size],
  p95: item_dates[0.95 * item_dates.size],
  p99: item_dates[0.99 * item_dates.size]
}

puts "Relation age distribution:"
puts JSON.pretty_generate(age_distribution)

File.write(out_file, json.to_json)
